#!/usr/bin/env python3

inventario = {}


def insertar(codigo: str, nombre: str, precio: float, cantidad: int):
    # inventario[codigo] = [nombre, precio, cantidad]
    valores = {"nombre": nombre, "precio": precio, "cantidad": cantidad}
    inventario[codigo] = valores


def listar():
    print("Código\tNombre\tPrecio\tCantidad")
    for codigo, valores in inventario.items():
        print(f"{codigo}\t{valores['nombre']}\t{valores['precio']}\t{valores['cantidad']}")


def consultar(codigo: str):
    if codigo in inventario:
        valores = inventario[codigo]
        print(f"Código: {codigo}")
        print(f"Nombre: {valores['nombre']}")
        print(f"Precio: {valores['precio']}")
        print(f"Cantidad: {valores['cantidad']}")
    else:
        print(f"El artículo con código {codigo} no existe en el inventario.")


def agotados():
    agotados_list = [codigo for codigo, valores in inventario.items() if valores['cantidad'] == 0]
    if agotados_list:
        print("Artículos agotados:")
        for codigo in agotados_list:
            print(f"{codigo}: {inventario[codigo]['nombre']}")
    else:
        print("No hay artículos agotados en el inventario.")


def pide_articulo() -> (str, str, float, int):
    codigo = input("Código del artículo: ")
    nombre = input("Nombre del artículo: ")
    precio = float(input("Precio del artículo: "))
    cantidad = int(input("Cantidad del artículo: "))
    return codigo, nombre, precio, cantidad


def menu() -> str:
    print("1. Insertar un artículo")
    print("2. Listar artículos")
    print("3. Consultar artículo")
    print("4. Artículos agotados")
    print("0. Salir")
    opcion = input("Opción: ")
    return opcion


def main():
    seguir = True
    while seguir:
        opcion = menu()
        if opcion == '0':
            seguir = False
        elif opcion == '1':
            codigo, nombre, precio, cantidad = pide_articulo()
            insertar(codigo, nombre, precio, cantidad)
            print("Artículo insertado correctamente.")
        elif opcion == '2':
            listar()
        elif opcion == '3':
            codigo = input("Ingrese el código del artículo a consultar: ")
            consultar(codigo)
        elif opcion == '4':
            agotados()
        else:
            print("Opción incorrecta")


if __name__ == '__main__':
    main()